package gcu.mpd.cw.s1513911.bgs;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {


    @Test
    public void earthquake_toString_isCorrect(){
        Earthquake earthquake = new Earthquake();
        earthquake.setDesclocation("Glasgow");
        earthquake.setDescdepth(4);
        earthquake.setMagnitude(4.4);
        earthquake.setLongitude(2.3);
        earthquake.setLatitude(32.4);


        assertEquals("4", earthquake.getDescdepth().toString());
        assertEquals("4.4", earthquake.getMagnitude().toString());
        assertEquals("Glasgow", earthquake.getDesclocation());
    }

    @Test
    public void earthquake_depth_isIncorrect(){
        Earthquake earthquake = new Earthquake();
        earthquake.setDesclocation("Glasgow");
        earthquake.setDescdepth(4);


        assertNotEquals("5", earthquake.getDescdepth().toString());
        assertEquals("Glasgow", earthquake.getDesclocation());
    }

    @Test
    public void earthquake_location_isIncorrect(){
        Earthquake earthquake = new Earthquake();
        earthquake.setDesclocation("Glasgow");
        earthquake.setDescdepth(4);


        assertEquals("4", earthquake.getDescdepth().toString());
        assertNotEquals("Glasgoww", earthquake.getDesclocation());
    }


}