//
// Name                 Thomas Murney
// Student ID           S1513911
// Programme of Study   Mobile Platform Development
//

package gcu.mpd.cw.s1513911.bgs;


import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> implements Filterable {


    private ArrayList<Earthquake> earthquakes;
    ArrayList<Earthquake> fullList = new ArrayList<>();



    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView result;
        CardView datacard;

        public ViewHolder(View view) {
            super(view);
            result = (TextView) view.findViewById(R.id.itemText);
            datacard = (CardView) view.findViewById(R.id.itemCard);

        }
    }


    public ListAdapter(ArrayList<Earthquake> earthquakes) {

        this.earthquakes = earthquakes;
        fullList = new ArrayList<>(earthquakes);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.result.setText(earthquakes.get(position).toString());
        //holder.result.setTextColor(Color.WHITE);
        boolean isSea = false;

        if(earthquakes.get(position).getDesclocation().contains("SEA") || earthquakes.get(position).getDesclocation().contains("FIRTH") || earthquakes.get(position).getDesclocation().contains("LOCH") || earthquakes.get(position).getDesclocation().contains("SOUND ") ){
            isSea = true;

        }
            if(earthquakes.get(position).getMagnitude() >= 3.0 && isSea){
                holder.result.setTextColor(Color.RED);
                holder.datacard.setBackgroundColor(Color.rgb(0, 0, 128));
            } else if(earthquakes.get(position).getMagnitude() >= 1.5 && isSea){
                holder.datacard.setBackgroundColor(Color.rgb(0, 191, 255));
            }



        if(earthquakes.get(position).getMagnitude() >= 3.0 && !isSea){
                //holder.result.setTextColor(Color.RED);
                holder.datacard.setBackgroundColor(Color.rgb(0, 128, 0));
            } else if (earthquakes.get(position).getMagnitude() >= 1.5 && !isSea){
                holder.datacard.setBackgroundColor(Color.rgb(60, 179, 113));
            } else if(earthquakes.get(position).getMagnitude() < 1.5 && !isSea){

                holder.datacard.setBackgroundColor(Color.rgb(138, 255, 138));
            }




    }
    @Override
    public Filter getFilter() {
        return listFilter;
    }

    private Filter listFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<Earthquake> filteredList = new ArrayList<>();


            if(constraint == null || constraint.length() == 0){
                filteredList.addAll(fullList);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for(Earthquake e: fullList){
                    if(e.getDesclocation().toLowerCase().contains(filterPattern)){
                        filteredList.add(e);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            earthquakes.clear();
            earthquakes.addAll((ArrayList) results.values);
            notifyDataSetChanged();
        }
    };
    @Override
    public int getItemCount() {
        return earthquakes.size();
    }


}
