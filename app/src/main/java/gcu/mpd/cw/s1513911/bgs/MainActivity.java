//
// Name                 Thomas Murney
// Student ID           S1513911
// Programme of Study   Mobile Platform Development
//


package gcu.mpd.cw.s1513911.bgs;

import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private SectionsPageAdapter mSessionsPageAdapter;
    private ViewPager mViewPager;



    XMLPullParserHandler.onParseListener listener;

    Bundle bundle;

    SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSessionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        XMLPullParserHandler dataParser = new XMLPullParserHandler(new XMLPullParserHandler.onParseListener() {

            public void onParseFinish(ArrayList<Earthquake> result) {
                ArrayList<Earthquake> data;
                data = result;

                Bundle bundle = new Bundle();

                //Log.e("Test", "TESTING ->" + result.get(9).getMagnitude());

                bundle.putParcelableArrayList("send", result);


                adapter.addFragment(new listFragment(), "LIST");
                adapter.getItem(0).setArguments(bundle);

                adapter.addFragment(new mapFragment(), "MAP");
                adapter.getItem(1).setArguments(bundle);

                adapter.addFragment(new statsFragment(), "STATS");
                adapter.getItem(2).setArguments(bundle);

                setupViewPager(mViewPager);


            }
        });


        dataParser.execute("http://quakes.bgs.ac.uk/feeds/MhSeismology.xml");


    }

    private void setupViewPager(ViewPager viewPager){



        viewPager.setAdapter(adapter);
    }


}

