package gcu.mpd.cw.s1513911.bgs;

//
// Name                 Thomas Murney
// Student ID           S1513911
// Programme of Study   Mobile Platform Development
//


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class statsFragment extends Fragment {
    private static final String TAG = "statsFragment";

    private Button btnTest;
    Bundle bundle = new Bundle();
    ArrayList<Earthquake> receivedData = new ArrayList<>();
    TextView highMag;
    TextView highDep;
    TextView highN;
    TextView highS;
    TextView highE;
    TextView highW;

    double mostE = 0;
    double mostW = 0;
    double mostN = 0;
    double mostS = 180;
    double baseMag = 0;
    Integer baseDep = 0;

    Earthquake mostMag;
    Earthquake deepest;
    Earthquake northern;
    Earthquake southern;
    Earthquake eastern;
    Earthquake western;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_stats,container, false);


        bundle = getArguments();
        receivedData = bundle.getParcelableArrayList("send");

        highMag = (TextView) view.findViewById(R.id.highMag);
        highDep = (TextView) view.findViewById(R.id.highDep);
        highN = (TextView) view.findViewById(R.id.highN);
        highS = (TextView) view.findViewById(R.id.highS);
        highE = (TextView) view.findViewById(R.id.highE);
        highW = (TextView) view.findViewById(R.id.highW);

        for(Earthquake eq : receivedData){
            if(eq.getLongitude() > mostE){
                mostE = eq.getLongitude();
                eastern = eq;

            }

            if(eq.getLongitude() < mostW){
                mostW = eq.getLongitude();
                western = eq;

            }

            if(eq.getLatitude() > mostN){
                mostN = eq.getLatitude();
                northern = eq;


            }

            if(eq.getLatitude() < mostS){
                mostS = eq.getLatitude();
                southern = eq;

            }

            if(eq.getMagnitude() > baseMag){
                baseMag = eq.getMagnitude();
                mostMag = eq;


            }

            if(eq.getDescdepth() > baseDep){
                baseDep = eq.getDescdepth();
                deepest = eq;


            }
        }

        Log.e("tag", "Most Eastern: " + eastern.getDesclocation());
        Log.e("tag", "Most Western: " + western.getDesclocation());
        Log.e("tag", "Most Northern: " + northern.getDesclocation());
        Log.e("tag", "Most Southern: " + southern.getDesclocation());
        Log.e("tag", "Strongest: " + mostMag.getDesclocation());
        Log.e("tag", "Deepest: " + deepest.getDesclocation());

        highMag.setText("" + mostMag.getDesclocation() + ", " + mostMag.getMagnitude());
        highDep.setText("" + deepest.getDesclocation() + ", " + deepest.getDescdepth() + "km");
        highN.setText("" + northern.getDesclocation() + ", " +  northern.getLatitude());
        highS.setText("" + southern.getDesclocation() + "," + southern.getLatitude());
        highE.setText("" + eastern.getDesclocation() + "," + eastern.getLongitude());
        highW.setText("" + western.getDesclocation() + "," + western.getLongitude());
        return view;
    }



}