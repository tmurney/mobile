//
// Name                 Thomas Murney
// Student ID           S1513911
// Programme of Study   Mobile Platform Development
//


package gcu.mpd.cw.s1513911.bgs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Map;

public class mapFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = "mapFragment";


    GoogleMap mGoogleMap;
    MapView mMapView;
    View mView;
    Bundle bundle = new Bundle();
    ArrayList<Earthquake> receivedData = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        mView = inflater.inflate(R.layout.mapfragment,container, false);


        bundle = getArguments();
        receivedData = bundle.getParcelableArrayList("send");

        return mView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMapView =  ((MapView) mView.findViewById(R.id.google_map_view));

        if(mMapView != null){
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());

        mGoogleMap = googleMap;

        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        for( Earthquake e: receivedData){
            googleMap.addMarker(new MarkerOptions().position(new LatLng(e.getLatitude(), e.getLongitude())).title(e.getDesclocation()).snippet("Magnitude: " + e.getMagnitude() + "  " + e.getDescdatetime()) );

        }


        CameraPosition Hame = CameraPosition.builder().target(new LatLng(55.8668, -4.25)).zoom(5).bearing(8).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(Hame));
    }
}
