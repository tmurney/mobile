//
// Name                 Thomas Murney
// Student ID           S1513911
// Programme of Study   Mobile Platform Development
//

package gcu.mpd.cw.s1513911.bgs;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import java.util.Date;

public class Earthquake implements Parcelable {
    private static final String TAG = "Earthquake";




    private Date descdatetime;
    private String desclocation;

    private Integer descdepth;
    private Double magnitude;

    private double latitude;
    private double longitude;

    protected Earthquake(Parcel in) {
        desclocation = in.readString();
        if (in.readByte() == 0) {
            descdepth = null;
        } else {
            descdepth = in.readInt();
        }
        if (in.readByte() == 0) {
            magnitude = null;
        } else {
            magnitude = in.readDouble();
        }
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public static final Creator<Earthquake> CREATOR = new Creator<Earthquake>() {
        @Override
        public Earthquake createFromParcel(Parcel in) {
            return new Earthquake(in);
        }

        @Override
        public Earthquake[] newArray(int size) {
            return new Earthquake[size];
        }
    };


    public Earthquake(){

    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Date getDescdatetime() {
        return descdatetime;
    }

    public void setDescdatetime(Date descdatetime) {
        this.descdatetime = descdatetime;
    }


    public Integer getDescdepth() {
        return descdepth;
    }

    public void setDescdepth(Integer descdepth) {
        this.descdepth = descdepth;
    }


    public String getDesclocation() {
        return desclocation;
    }

    public void setDesclocation(String desclocation) {
        this.desclocation = desclocation;
    }

    public Double getMagnitude() {
        return magnitude;
    }

    public void setMagnitude(Double magnitude) {
        this.magnitude = magnitude;
    }

    @Nullable
    public String toString(){
        return " Date/Time: " + descdatetime + "\n Location: " + desclocation +  "\n Depth: " + descdepth + "km \n Magnitude: " + magnitude;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(desclocation);
        if (descdepth == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(descdepth);
        }
        if (magnitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(magnitude);
        }
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }
}
