//
// Name                 Thomas Murney
// Student ID           S1513911
// Programme of Study   Mobile Platform Development
//

package gcu.mpd.cw.s1513911.bgs;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.lang.reflect.Array;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;



public class XMLPullParserHandler extends AsyncTask<String, Void, ArrayList> {

    public interface onParseListener {
        void onParseFinish(ArrayList<Earthquake> arrayList);
    }

    public ArrayList<Earthquake> Earthquakes = new ArrayList<>();
    private Earthquake eq = null;

    public onParseListener delegate = null;

    public XMLPullParserHandler(onParseListener listener){

        delegate = listener;
    }

    @Override
    protected ArrayList doInBackground(String... strings) {

        URL aurl;
        URLConnection yc;
        BufferedReader in;


        Log.e("Connected","Starting XML Pull..");

        int count = strings.length;
        long totalSize = 0;
        for (int i = 0; i < count; i++) {

            try {

                aurl = new URL(strings[i]);
                yc = aurl.openConnection();
                in = new BufferedReader(new InputStreamReader(yc.getInputStream()));

                boolean isItem = false;

                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser xpp = factory.newPullParser();
                xpp.setInput(in);
                int eventType = xpp.getEventType();

                while (eventType != XmlPullParser.END_DOCUMENT) {
                    // Found a start tag
                    if (eventType == XmlPullParser.START_TAG) {
                        if (xpp.getName().equalsIgnoreCase("channel")) {
                            Earthquakes = new ArrayList<>();
                        }
                        if (xpp.getName().equalsIgnoreCase("item")) {
                            eq = new Earthquake();
                            isItem = true;


                        } else if (xpp.getName().equalsIgnoreCase("description") && isItem) {
                            String text = xpp.nextText();
                            String desc[] = text.split(" ; ");
                            String location = desc[1];

                            Integer local_length = location.length();
                            String magnitude = desc[4].substring(11, 15);

                            String datestring = desc[0].substring(18, 43);
                            SimpleDateFormat dateFormat = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss", Locale.UK);
                            try {
                                Date date = dateFormat.parse(datestring);
                                eq.setDescdatetime(date);
                            } catch (java.text.ParseException e) {
                                e.printStackTrace();
                            }


                            String depth = desc[3].substring(7, desc[3].indexOf(" ", 8));

                            String lat = desc[2].substring(10, 16);
                            String lon = desc[2].substring(17, desc[2].length());

                            eq.setDesclocation(location.substring(10, local_length));
                            eq.setDescdepth(Integer.parseInt(depth));
                            eq.setMagnitude(Double.parseDouble(magnitude));
                            eq.setLatitude(Double.parseDouble(lat));
                            eq.setLongitude(Double.parseDouble(lon));

                        }
                    } else if (eventType == XmlPullParser.END_TAG) {
                        if (xpp.getName().equalsIgnoreCase("item") && isItem) {

                            Earthquakes.add(eq);
                            isItem = false;

                        }
                        if (xpp.getName().equalsIgnoreCase("channel")) {

                            int size;
                            size = Earthquakes.size();
                            Log.e("List size", "size is " + size);
                        }

                    }

                    eventType = xpp.next();
                }


            } catch (IOException ae) {
                Log.e("ERROR", "ioexception");
            } catch (XmlPullParserException e) {
                Log.e("ERROR", "xmlpullparserexception");
            }
        }

        return Earthquakes;
    }

    @Override
    protected void onPostExecute(ArrayList result) {
        delegate.onParseFinish(result);
        super.onPostExecute(result);
    }



}
