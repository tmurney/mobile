//
// Name                 Thomas Murney
// Student ID           S1513911
// Programme of Study   Mobile Platform Development
//


package gcu.mpd.cw.s1513911.bgs;

import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;

import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class listFragment extends Fragment {
    private static final String TAG = "listFragment";

    private Button btnTest;
    private TextView txtDisplay;

    Bundle bundle = new Bundle();
    ArrayList<Earthquake> receivedData = new ArrayList<>();
    ArrayList<Earthquake> fullList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.listfragment, container, false);

        SearchView searchView = (SearchView) view.findViewById(R.id.search);
        RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.resultView);

        searchView.setMaxWidth(Integer.MAX_VALUE);


        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        bundle = getArguments();
        receivedData = bundle.getParcelableArrayList("send");


        final ListAdapter mAdapter = new ListAdapter(receivedData);
        mRecyclerView.setAdapter(mAdapter);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return view;

    }


}





